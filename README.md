# HandyKaraoke

#### การติดตั้ง
  โหลดไฟล์จาก [Releases](https://github.com/pie62/HandyKaraoke/releases) แยกไฟล์ แล้วดับเบิ้ลคลิกไฟล์ HandyKaraoke เพื่อเปิดใช้งานได้เลย
  
#### คีย์ลัด
  * Ctrl+ลูกศรขึ้น-ลง   =   ปรับระดับเสียง เพิ่ม-ลด
  * ลูกศรซ้าย-ขวา   =   ค้นหาเพลงต่อไป-เพลงก่อนหน้า
  * ลูกศรขึ้น-ลง    =   เลือกเพลงในรายการเล่น
  * PageUp-PageDown   =   ปรับตำแหน่งเพลงในรายการเพลงขึ้นลง
  * Insert    =   เพิ่มคีย์เพลง
  * Delete    =   ลดคีย์เพลง
  * Spacebar    =   ดูรายละเอียดเพลงที่กำลังเล่น
  * Tab   =   เปลี่ยนเป็นค้นหาจาก ชื่อเพลง,ชื่อศิลปิน,รหัสเพลง...(เมื่อกำลังค้นหาเพลง)

#### วิธีการคอมไพล์
  * sudo apt install qt5-default qt5-qmake build-essential
  * git clone https://gitlab.com/semiauto/HandyKaraoke
  * cd HandyKaraoke
  * mkdir build
  * cd build
  * qmake ../HandyKaraoke.pro
  * make
  * make clean
  * ดับเบิลคลิกที่ไฟล์ HandyKaraoke ได้เลย

