#!/bin/bash

mkdir lib
cp ../BASS/bass24-linux/libbass.so ./lib/.
cp ../BASS/bass_fx24-linux/libbass_fx.so ./lib/.
cp ../BASS/bassmidi24-linux/libbassmidi.so ./lib/.
cp ../BASS/bassmix24-linux/libbassmix.so ./lib/.
