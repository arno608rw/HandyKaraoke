#!/bin/bash

mkdir lib
cp ../BASS/bass24-linux/aarch64/libbass.so ./lib/.
cp ../BASS/bass_fx24-linux/aarch64/libbass_fx.so ./lib/.
cp ../BASS/bassmidi24-linux/aarch64/libbassmidi.so ./lib/.
cp ../BASS/bassmix24-linux/aarch64/libbassmix.so ./lib/.
