#!/bin/bash

mkdir lib
cp ../BASS/bass24-linux/armhf/libbass.so ./lib/.
cp ../BASS/bass_fx24-linux/armhf/libbass_fx.so ./lib/.
cp ../BASS/bassmidi24-linux/armhf/libbassmidi.so ./lib/.
cp ../BASS/bassmix24-linux/armhf/libbassmix.so ./lib/.
